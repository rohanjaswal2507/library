package admin;

import helpers.InfoFetcher;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class IssuedBooks
 */
public class IssuedBooks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IssuedBooks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw = response.getWriter();
	    InfoFetcher jarvis = new InfoFetcher();
	    
	    try{
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			Date currentDate = new Date();
			java.sql.Date sqlCurrentDate = new java.sql.Date(currentDate.getTime());
			Statement s=con.createStatement();
			ResultSet result = s.executeQuery("select * from issued_books where return_date is null"); 
	        String msg=" ";
	        List<IssuedBook> books = new ArrayList<IssuedBook>();
	        while(result.next()){
	        	System.out.println("a record found!");
	        	IssuedBook book = new IssuedBook();
	        	book.bookId = result.getInt(2);
	        	book.userId = result.getString(3);
	        	book.issueDate = result.getDate(4);
	        	book.scheduledReturnDate = result.getDate(5);
	        	book.title = jarvis.bookById(book.bookId).title;
	        	books.add(book);
	        }
	        request.setAttribute("issuedBooks", books);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/viewIssuedBooks.jsp");
			dispatcher.forward(request, response);
	        s.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
