 package helpers;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.Book;

/**
 * Servlet implementation class SearchServlet
 */
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Searching for the book");
		String q = request.getParameter("q");
		InfoFetcher jarvis = new InfoFetcher();
		List<Book> books = new ArrayList<Book>();
		books = jarvis.booksByName(q);
		for (int i = 0; i<books.size(); i++){
			Book book = (Book)books.get(i);
			System.out.println(book.title);
			book.availability = jarvis.checkAvailability(book.isbn);
			if (book.availability == 0){
				book.earliest_availability_date = jarvis.earliestReturnDate(book.isbn);
			} else {
				book.earliest_availability_date = new Date();
				System.out.println(book.earliest_availability_date);
			}
			
		}
		request.setAttribute("books", books);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/search.jsp");
		dispatcher.forward(request, response);
	}

}
