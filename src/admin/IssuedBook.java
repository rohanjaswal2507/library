package admin;

import java.util.Date;

public class IssuedBook {
	public String title;
	public int bookId;
	public String userId;
	public String author;
	public String username;
	public Date issueDate;
	public Date scheduledReturnDate;
	public Date returnDate;
}
