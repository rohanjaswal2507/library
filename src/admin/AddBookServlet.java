package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class AddBookServlet
 */
public class AddBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddBookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw=response.getWriter();
		int isbn=Integer.parseInt(request.getParameter("isbn"));
		int id=Integer.parseInt(request.getParameter("id"));
		String title=request.getParameter("title");
		String author=request.getParameter("author");
		String publisher=request.getParameter("publisher");
		
		try{
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			PreparedStatement s=con.prepareStatement("insert into books(isbn,id,title,author,publisher) values(?,?,?,?,?)");
			s.setInt(1,isbn);
			s.setInt(2,id);
			s.setString(3,title);
			s.setString(4,author);
			s.setString(5,publisher);
			int i = s.executeUpdate();
			con.commit(); 
	        String msg=" ";
	        if(i!=0){  
	        	msg="Record has been inserted";
	        	pw.println("<font size='6' color=blue>" + msg + "</font>");  
	        }  
	        else{  
	          msg="failed to insert the data";
	          pw.println("<font size='6' color=blue>" + msg + "</font>");
	         }  
	        s.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
