<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<title>Return Book - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
		<div class="container-fluid">
			<div class="row">
				<jsp:include page="../sidebar.html"></jsp:include>
				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
 					<h1>Return a Book</h1>
 					<div class="row">
 						<div class="col-md-2"></div>
 						<div class="col-md-6">
 							<form action="/Library/ReturnBook" method="post">
  								<div class="form-group">
    								<label for="Book_id">Book Id:</label>
    								<input type="text" class="form-control" id="email" name="book_id">
  								</div>
  								<div class="form-group">
    								<label for="user_id">User Id:</label>
    								<input type="text" class="form-control" id="pwd" name="user_id">
  								</div>
  								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
 						</div>
 					</div>
        		</div>
			</div>
		</div>
</body>
</html>