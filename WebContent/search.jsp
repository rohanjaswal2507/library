<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="admin.Book" %>
<%@ page import="java.util.*" %>
<jsp:include page = "header.html"/>
<title><%=request.getParameter("q") %> - NITH Library</title>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<div class="container-fluid">
		<div class="row">
			<h4>Search Results for <%=request.getParameter("q") %></h4>
		</div>
		<div class = "row">
			<div class = "col-md-8">
				 <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Publisher</th>
                  <th>Status</th>
                  <th>Expected availability Date</th>
                </tr>
              </thead>
              <tbody>
              	<% List<Book> books = new ArrayList<Book>();
					books = (List<Book>)request.getAttribute("books");
					for (int i = 0; i < books.size(); i++){
						Book book = new Book();
						book = books.get(i);
				%>
              	<tr>
                  <td><%= (i+1)%></td>
                  <td><%= book.title %></td>
                  <td><%=book.author %></td>
                  <td><%=book.publisher %></td>
                  <td><%if (book.availability > 0) {out.println("Available");} else {out.println("Not Available");} %></td>
                  <td><%=book.earliest_availability_date %></td>
                </tr>
                <%} %>
              </tbody>
            </table>
          </div>
			</div>
		</div>
	</div>
</body>
</html>