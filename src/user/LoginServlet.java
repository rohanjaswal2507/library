package user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uname=request.getParameter("username");
		String pass=request.getParameter("password");
		System.out.println("Validating Login");
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			HttpSession session = request.getSession();
			if (session.getAttribute("isLoggedIn") != null){
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/home.jsp");
				dispatcher.forward(request, response);
			}
			PreparedStatement userFetch = con.prepareStatement("select username from users where username=? and password=?");
			userFetch.setString(1, uname);
			userFetch.setString(2, pass);
			ResultSet result = userFetch.executeQuery();
			if (result.next()){
				session.setAttribute("username", (String)result.getString(1));
				session.setAttribute("isLoggedIn", "true");
				RequestDispatcher dis = getServletContext().getRequestDispatcher("/userHome.jsp");
				dis.forward(request, response);
			} else {
				RequestDispatcher dis = getServletContext().getRequestDispatcher("/login.jsp");
				request.setAttribute("msg", "Invalid login credentials!");
				dis.forward(request, response);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
