<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page = "validateAdminLogin.jsp"/>
<%@ page import="helpers.InfoFetcher" %>
<%@ page import="admin.*" %>
<%@ page import="java.util.*" %>
<jsp:include page = "../header.html"/>
<title>My History - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
	<div class="container-fluid">
    	<div class="row">
   			<jsp:include page="../sidebar.html"></jsp:include>
   			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="sub-header">History of Issued Books</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Issued On</th>
                  <th>Issued To</th>
                  <th>Scheduled Return Date</th>
                  <th>Return Date</th>
                </tr>
              </thead>
              <tbody>
              <% InfoFetcher jarvis = new InfoFetcher();
              	List<IssuedBook> books = jarvis.booksHistory();
              	for ( int i = 0; i < books.size(); i++){
              		IssuedBook book = books.get(i);
              %>
                <tr>
                  <td><%=(i+1)%></td>
                  <td><%=book.title%></td>
                  <td><%=book.author%></td>
                  <td><%= book.issueDate%></td>
                  <td><%=book.userId %></td>
                  <td><%=book.scheduledReturnDate%></td>
                  <td><%=book.returnDate %></td>
                </tr>
              <% } %>
              </tbody>
            </table>
          </div>
        </div>
   		</div>
   	</div>
</body>
</html>