<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<%@ page import ="admin.Book"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete a Book - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
	<div class="container">
		<div class="row">
			<h3>Please confirm the details</h3>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<table class="table table-responsive table-bordered">
					<thead>
                		<tr>
                 			<th></th>
                  			<th></th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		<%
                  			Book book = (Book)request.getAttribute("book");
                  			if(request.getAttribute("param").equals("isbn")){
                  				out.println("<font color=red>Deleting a book by ISBN will delete all copies of it.</font>");
                  			}
                  		%>
                  		<tr>
                  			<td><strong>ISBN</strong></td>
                  			<td><%=book.isbn %></td>
						</tr>
						<tr>
                  			<td><strong>Title</strong></td>
                  			<td><%=book.title %></td>
						</tr>
						<tr>
                  			<td><strong>Author</strong></td>
                  			<td><%=book.author %></td>
						</tr>
						<tr>
                  			<td><strong>Publisher</strong></td>
                  			<td><%=book.publisher %></td>
						</tr>
                  	</tbody>
				</table>
				<a href="/Library/ConfirmedDeleteServlet?param=<%=request.getAttribute("param") %>&val=<%=request.getAttribute("val")%>">
					<button type="submit" class="btn btn-lg btn-primary">Delete</button>
				</a>
				<a href="/admin/home.jsp">
					<button type="reset" class="btn btn-lg btn-warning">Cancel</button>
				</a>
			</div>
		</div>
	</div>
</body>
</html>