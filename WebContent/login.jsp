<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page = "header.html"/>
<style>
span.psw {
    float: right;
    padding-top: -2px;
}
</style>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container">
		<div class = "row">
			<h1>Welcome to library portal</h1>
			<br/><br/>
		</div>
		<div class="col-md-4 col-md-offset-4">
			<% if(request.getAttribute("msg") != null) {
				out.println("<font color=red size=5>" + request.getAttribute("msg") + "</font>");
			}
			%>

			<form action="/Library/LoginServlet" method="post">
				<div class="form-group">
    				<label><b>Username:</b></label>
    				<input type="text" class="form-control" placeholder="Enter Username" name="username">
    			</div>
    			<div class="form-group">
    				<label><b>Password:</b></label>
    				<input type="password" class="form-control" placeholder="Enter Password" name="password">
    			</div>
    			<button type="submit" class="btn btn-primary">Submit</button><br><br>
    			  <input type="checkbox" checked="checked"> Remember me
    			  <span class="psw">new user <a href="newUser.jsp">signIn</a></span>
			</form>
		</div>
	</div>

</body>
</html>