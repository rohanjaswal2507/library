<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="admin.IssuedBook" %>
<%@ page import="java.util.*" %>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<title>Issued Books - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
	 <div class="container-fluid">
      <div class="row">
        <jsp:include page="../sidebar.html"></jsp:include>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<h1>Issued Books</h1>
        	<table class="table ">
 								<thead>
 									<tr>
 										<th>#</th>
 										<th>Title</th>
 										<th>Book ID</th>
 										<th>User ID</th>
 										<th>Issue Date</th>
 										<th>Scheduled Return Date</th>
 									</tr>
 								</thead>
 								<tbody>
 									<% List<IssuedBook> books = new ArrayList<IssuedBook>();
										books = (List<IssuedBook>)request.getAttribute("issuedBooks");
										for (int i = 0; i < books.size(); i++){
											IssuedBook book = new IssuedBook();
											book = books.get(i);
									%>
              						<tr>
                  						<td><%= (i+1)%></td>
                  						<td><%= book.title %></td>
                  						<td><%=book.bookId %></td>
                  						<td><%=book.userId %></td>
                  						<td><%=book.issueDate %></td>
                  						<td><%=book.scheduledReturnDate %></td>
                						</tr>
                					<%} %>
 								</tbody>
 							</table>
        </div>  
      </div>
    </div>
</body>
</html>