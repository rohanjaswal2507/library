package admin;

import helpers.InfoFetcher;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class ReturnBook
 */
public class ReturnBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReturnBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int bookId = Integer.parseInt(request.getParameter("book_id"));
		String userId = request.getParameter("user_id");
		Date currentDate = new Date();
	    PrintWriter pw = response.getWriter();
		
	    try{
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			PreparedStatement s=con.prepareStatement("update issued_books set return_date=? where book_id=? and return_date is null");
			java.sql.Date sqlCurrentDate = new java.sql.Date(currentDate.getTime());
			s.setDate(1, sqlCurrentDate);
			s.setInt(2,bookId);
			int i = s.executeUpdate();
			con.commit(); 
	        String msg=" ";
	        if(i!=0){  
	        	msg="Book has been returned!";
	        	pw.println("<font size='6' color=blue>" + msg + "</font>");
	        }  
	        else{  
	          msg="No such record found!";
	          pw.println("<font size='6' color=blue>" + msg + "</font>");
	         }  
	        s.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
