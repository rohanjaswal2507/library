<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a Book - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
	<div class="container">
		<div class="row">
			<h3>Please confirm the details</h3>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
			<form action="/Library/AddBookServlet" method="post">
				<table class="table table-responsive table-bordered">
					<thead>
                		<tr>
                 			<th></th>
                  			<th></th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		<tr>
                  			<td><strong>ISBN</strong></td>
                  			<td><input type="text" name="isbn" value=<%=request.getParameter("isbn") %>></td>
						</tr>
						<tr>
                  			<td><strong>ID</strong></td>
                  			<td><input type="text" name="id" value=<%=request.getParameter("id") %>></td>
						</tr>
						<tr>
                  			<td><strong>Title</strong></td>
                  			<td><input type="text" name="title" value="<%=(String)request.getParameter("title") %>"></td>
						</tr>
						<tr>
                  			<td><strong>Author</strong></td>
                  			<td><input type="text" name="author" value="<%=request.getParameter("author") %>"></td>
						</tr>
						<tr>
                  			<td><strong>Publisher</strong></td>
                  			<td><input type="text" name="publisher" value="<%=request.getParameter("publisher") %>"></td>
						</tr>
                  	</tbody>
				</table>
				<input type="submit" class="btn btn-lg btn-primary" ></button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>