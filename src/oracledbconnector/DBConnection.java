package oracledbconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	public static Connection con = null;
	public Connection getConnection() throws SQLException{
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@admin-PC:1521:xe","hr","hr");
			System.out.println("Connection to the database successful!");
		} catch (ClassNotFoundException e){
			System.out.println("Class not found!");
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return con;
	}
}