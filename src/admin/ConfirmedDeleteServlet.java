package admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class ConfirmedDeleteServlet
 */
public class ConfirmedDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmedDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String param = request.getParameter("param");
		String val = request.getParameter("val");
		System.out.println(param+val);
		String query = "";
		HttpSession session = request.getSession(false);
		if (param.equals("isbn")){
			query = "delete from books where isbn = " + val;
		} else if (param.equals("id")){
			query = "delete from books where id = " + val;
		}
		if (session.getAttribute("isAdmin").equals("true")){
			try {
				DBConnection connector = new DBConnection();
				Connection con = connector.getConnection();
				//delete details from database
				Statement bookDelete = con.createStatement();
				int i = bookDelete.executeUpdate(query);
				if (i > 0){
					request.setAttribute("result", "true");
					RequestDispatcher dis = getServletContext().getRequestDispatcher("/admin/deleteBooks.jsp");
					dis.forward(request, response);
				} else {
					request.setAttribute("result", "false");
					RequestDispatcher dis = getServletContext().getRequestDispatcher("/admin/deleteBooks.jsp");
					dis.forward(request, response);
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
