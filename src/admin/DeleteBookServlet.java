package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class DeleteBookServlet
 */
public class DeleteBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteBookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		int isbn=0;
		int id=0;
		String query = "";
		if(!(request.getParameter("isbn").equals(""))){
			isbn = Integer.parseInt(request.getParameter("isbn"));
			query = "select distinct isbn, id, title, author, publisher from books where isbn=" + isbn;
			request.setAttribute("param", "isbn");
			request.setAttribute("val", isbn);
			System.out.println(request.getAttribute("val"));
		} else if (!(request.getParameter("id").equals(""))){
			id = Integer.parseInt(request.getParameter("id"));
			query = "select * from books where id=" + id;
			request.setAttribute("param", "id");
			request.setAttribute("val", id);
		}
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve details from database
			Statement bookFetch = con.createStatement();
			ResultSet result = bookFetch.executeQuery(query);
			if(result.next()){
				System.out.println("book found in the database");
				Book book = new Book();
				book.title = result.getString(3);
				book.isbn = result.getInt(1);
				book.id = result.getInt(2);
				book.author = result.getString(4);
				book.publisher = result.getString(5);
				request.setAttribute("book", book);
				RequestDispatcher dis = getServletContext().getRequestDispatcher("/admin/deleteConfirm.jsp");
				dis.forward(request, response);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
