create table issued_books (
	isbn int not null,
	book_id int not null,
	user_id int not null,
	issue_date date not null,
	scheduled_return_date date,
	return_date date
)