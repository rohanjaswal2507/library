<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a Book - NITH Library</title>
</head>
<body>
	<jsp:include page="../navbar.jsp"/>
	<div class="container-fluid">
		<div class = "row">
		<jsp:include page="../sidebar.html"></jsp:include>
		<div class="col-md-2 main"></div>
		<div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1>Add a book</h1>
			<form action="AddBookConfirm.jsp" method="post">
				<div class="form-group">
    				<label><b>ISBN:</b></label>
    				<input type="text" class="form-control" placeholder="ISBN" name="isbn" required>
    			</div>
    			<div class="form-group">
    				<label><b>Book Id:</b></label>
    				<input type="text" class="form-control" placeholder="Book Id" name="id" required>
    			</div>
    			<div class="form-group">
   					<label><b>Title:</b></label>
    				<input type="text" class="form-control" placeholder="Book Title" name="title" required>
    			</div>
    			<div class="form-group">
    				<label><b>Author:</b></label>
    				<input type="text" class="form-control" placeholder="Author Name" name="author" required>
    			</div>
    			<div class="form-group">
    				<label><b>Publisher:</b></label>
    				<input type="text" class="form-control" placeholder="Publisher Name" name="publisher" required>
   				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</body>
</html>