package admin;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class AdminLoginServlet
 */
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uname=request.getParameter("username");
		String pass=request.getParameter("password");
		
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			HttpSession adminSession = request.getSession();
			if (uname.equals("admin") && pass.equals("admin")){
				adminSession.setAttribute("isLoggedIn", "true");
				adminSession.setAttribute("isAdmin", "true");
				RequestDispatcher dis = getServletContext().getRequestDispatcher("/admin/home.jsp");
				dis.forward(request, response);
			} else {
				adminSession.setAttribute("isLoggedIn", "false");
				RequestDispatcher dis = getServletContext().getRequestDispatcher("/login.jsp");
				request.setAttribute("msg", "Invalid login credentials!");
				dis.forward(request, response);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
