<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page = "validateAdminLogin.jsp"/>
<jsp:include page = "../header.html"/>
<title>Delete Books - NITH Library</title>
<body>
<jsp:include page="../navbar.jsp"/>
	<div class="container-fluid">
		<div class = "row">
			<jsp:include page="../sidebar.html"></jsp:include>
			<h2>Delete Books</h2>
		<div class="col-md-6 col-md-offset-2">
		<%
			try {
				if(request.getAttribute("result").equals("true"))
					out.println("<font color=green>Book deleted from the database</font>");
				else if (request.getAttribute("result").equals("false"))
					out.println("<font color=red>Failed to delete the book</font>");
			} catch (Exception e){
				//do nothing
			}
		%>
			<form action="/Library/DeleteBookServlet" method="get">
				<div class="form-group">
    				<label><b>ISBN:</b></label>
    				<input type="text" class="form-control" placeholder="ISBN" name="isbn">
    			</div>
    			<h3>OR</h3>
    			<div class="form-group">
    				<label><b>Book Id:</b></label>
    				<input type="text" class="form-control" placeholder="Book Id" name="id">
    			</div>
    			<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</body>
</html>