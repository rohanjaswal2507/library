package admin;

import java.sql.*;

public class Book {
	public int isbn;
	public int id;
	public String title;
	public String author;
	public String publisher;
	public Date issue_date;
	public int total_copies;
	public int availability;
	public java.util.Date earliest_availability_date;
	
	public int getId(){
		return this.id;
	}
}
