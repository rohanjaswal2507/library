package admin;

import helpers.InfoFetcher;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracledbconnector.DBConnection;

/**
 * Servlet implementation class IssueBook
 */
public class IssueBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IssueBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int bookId = Integer.parseInt(request.getParameter("book_id"));
		String userId = request.getParameter("user_id");
		Date currentDate = new Date();
		Date scheduledReturnDate = new Date();
		Calendar myCal = Calendar.getInstance();
	    myCal.setTime(currentDate);    
	    myCal.add(Calendar.MONTH, +1);    
	    scheduledReturnDate = myCal.getTime();
		
	    //retrieve isbn for the book
	    InfoFetcher jarvis = new InfoFetcher();
	    Book book = jarvis.bookById(bookId);
	    PrintWriter pw = response.getWriter();
	    
	    try{
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			PreparedStatement s=con.prepareStatement("insert into issued_books(isbn,book_id,user_id,issue_date,scheduled_return_date) values(?,?,?,?,?)");
			s.setInt(1,book.isbn);
			s.setInt(2,bookId);
			s.setString(3,userId);
			java.sql.Date sqlCurrentDate = new java.sql.Date(currentDate.getTime());
			s.setDate(4,sqlCurrentDate);
			java.sql.Date sqlScheduledReturnDate = new java.sql.Date(scheduledReturnDate.getTime());
			s.setDate(5,sqlScheduledReturnDate);
			int i = s.executeUpdate();
			con.commit(); 
	        String msg=" ";
	        if(i!=0){  
	        	msg="Record has been inserted";
	        	pw.println("<font size='6' color=blue>" + msg + "</font>");
	        }  
	        else{  
	          msg="failed to insert the data";
	          pw.println("<font size='6' color=blue>" + msg + "</font>");
	         }  
	        s.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
