create table books (
	isbn int not null,
	id int not null,
	title varchar2(25) not null,
	author varchar2(25),
	publisher varchar2(25),
	issue_date Date,
	total_copies int
)