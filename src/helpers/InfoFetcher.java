package helpers;

import java.sql.*;
import java.sql.Date;
import java.util.*;

import admin.*;
import oracledbconnector.DBConnection;
import user.User;

public class InfoFetcher {
	//All the helper functions to retrieve information about Users
	// and books from the database
	
	public Book bookById(int id){
		ResultSet result = null;
		Book foundBook = new Book();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			System.out.println("Searching a Book by its ID");
			PreparedStatement bookFetch;
			bookFetch = con.prepareStatement("select * from books where id=?");
			bookFetch.setInt(1, id);
			result = bookFetch.executeQuery();
			while(result.next()){
				foundBook.title = result.getString(3);
				foundBook.isbn = result.getInt(1);
				System.out.println(foundBook.title);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return foundBook;
	}
	
	public Book bookByIsbn(int isbn){
		ResultSet result = null;
		Book foundBook = new Book();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			PreparedStatement bookFetch;
			bookFetch = con.prepareStatement("select distinct isbn, title, author, publisher from books where id=?");
			bookFetch.setInt(1, isbn);
			result = bookFetch.executeQuery();
			while(result.next()){
				foundBook.title = result.getString(2);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return foundBook;
	}
	public User userById(int id){
		ResultSet result = null;
		User foundUser = new User();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			PreparedStatement bookFetch;
			bookFetch = con.prepareStatement("select * from users where user_id=?");
			bookFetch.setInt(1, id);
			result = bookFetch.executeQuery();
			while(result.next()){
				foundUser.name = result.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return foundUser;
	}
	
	public List booksByName(String name){
		ResultSet result = null;
		List books = new ArrayList();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			System.out.println("Searching for " + name);
			Statement booksFetch = con.createStatement();
			result = booksFetch.executeQuery("select distinct isbn, title, author, publisher from books where title like '%" + name + "%'");
			while(result.next()){
				Book foundBook = new Book();
				foundBook.title = result.getString(2);
				System.out.println(foundBook.title);
				foundBook.author = result.getString(3);
				foundBook.publisher = result.getString(4);
				foundBook.isbn = result.getInt(1);
				books.add(foundBook);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return books;
	}
	
	public int checkAvailability(int isbn){
		ResultSet result = null;
		int availableCopies = 0, totalCopies = 0, issuedCopies = 0;
		
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			PreparedStatement bookFetch;
			bookFetch = con.prepareStatement("select count(isbn) from books where isbn=?");
			bookFetch.setInt(1, isbn);
			result = bookFetch.executeQuery();
			while(result.next()){
				totalCopies = result.getInt(1);
				//System.out.println(totalCopies);
			}
			bookFetch = con.prepareStatement("select count(isbn) from issued_books where isbn=? and return_date is null");
			bookFetch.setInt(1, isbn);
			result = bookFetch.executeQuery();
			while(result.next()){
				issuedCopies = result.getInt(1);
				//System.out.println(issuedCopies);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return totalCopies - issuedCopies;
		
	}
	
	public Date earliestReturnDate(int isbn){
		ResultSet result = null;
		Book foundBook = new Book();
		long millis = System.currentTimeMillis();
		millis = millis * 2;
		java.util.Date returnDate = new java.util.Date(millis);
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			PreparedStatement bookFetch;
			bookFetch = con.prepareStatement("select * from issued_books where isbn=?");
			bookFetch.setInt(1, isbn);
			result = bookFetch.executeQuery();
			java.util.Date current_date = new java.util.Date();
			while(result.next()){
				foundBook.issue_date = result.getDate(5);
				if (foundBook.issue_date.compareTo(returnDate) < 0){
					returnDate = foundBook.issue_date;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return (Date) returnDate;
	}
	
	public List booksIssuedToUser(String username){
		ResultSet result = null;
		List books = new ArrayList();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			System.out.println("looking for books issued to " + username);
			Statement booksFetch = con.createStatement();
			result = booksFetch.executeQuery("select distinct * from issued_books join books on issued_books.book_id=books.id where user_id='" + username + "' and return_date is null order by scheduled_return_date");
			
			while(result.next()){
				IssuedBook foundBook = new IssuedBook();
				foundBook.title = result.getString(9);
				System.out.println(foundBook.title);
				foundBook.author = result.getString(10);
				foundBook.issueDate = result.getDate(4);
				foundBook.scheduledReturnDate = result.getDate(5);
				books.add(foundBook);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return books;
	}
	
	public List userBooksHistory(String username){
		ResultSet result = null;
		List books = new ArrayList();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			System.out.println("looking for books issued to " + username);
			Statement booksFetch = con.createStatement();
			result = booksFetch.executeQuery("select distinct * from issued_books join books on issued_books.book_id=books.id where user_id='" + username + "' order by scheduled_return_date");
			
			while(result.next()){
				IssuedBook foundBook = new IssuedBook();
				foundBook.title = result.getString(9);
				System.out.println(foundBook.title);
				foundBook.author = result.getString(10);
				foundBook.issueDate = result.getDate(4);
				foundBook.scheduledReturnDate = result.getDate(5);
				foundBook.returnDate = result.getDate(6);
				books.add(foundBook);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return books;
	}
	
	public List booksHistory(){
		ResultSet result = null;
		List books = new ArrayList();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			//retrieve password from database
			Statement booksFetch = con.createStatement();
			result = booksFetch.executeQuery("select distinct * from issued_books join books on issued_books.book_id=books.id order by scheduled_return_date");
			
			while(result.next()){
				IssuedBook foundBook = new IssuedBook();
				foundBook.title = result.getString(9);
				System.out.println(foundBook.title);
				foundBook.author = result.getString(10);
				foundBook.issueDate = result.getDate(4);
				foundBook.scheduledReturnDate = result.getDate(5);
				foundBook.returnDate = result.getDate(6);
				foundBook.userId = result.getString(3);
				books.add(foundBook);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return books;
	}
	
	public List allBooks(){
		ResultSet result = null;
		List books = new ArrayList();
		try {
			DBConnection connector = new DBConnection();
			Connection con = connector.getConnection();
			Connection con2 = connector.getConnection();
			//retrieve password from database
			Statement booksFetch = con.createStatement();
			Statement countFetch = con2.createStatement();
			result = booksFetch.executeQuery("select distinct isbn, title, author, publisher from books");
			
			while(result.next()){
				Book foundBook = new Book();
				foundBook.title = result.getString(2);
				System.out.println(foundBook.title);
				foundBook.author = result.getString(3);
				foundBook.publisher = result.getString(4);
				foundBook.isbn = result.getInt(1);
				ResultSet count = countFetch.executeQuery("select count(isbn) from books where isbn=" + foundBook.isbn);
				if(count.next()){
					foundBook.total_copies = count.getInt(1);
					//System.out.println("total: " + foundBook.total_copies);
				}
				books.add(foundBook);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return books;
	}
	
}
